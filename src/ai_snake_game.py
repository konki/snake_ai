from dataclasses import dataclass, field
from enum import Enum, auto
from typing import Optional

import numpy as np
import pygame as pg

pg.init()

BLOCK_SIZE: int = 40

# Colors
WHITE: tuple = (255, 255, 255)
BLACK: tuple = (0, 0, 0)
RED: tuple = (255, 0, 0)
GREEN: tuple = (0, 255, 0)
BLUE: tuple = (0, 0, 255)
BLUE1: tuple = (0, 100, 255)

# Font
font = pg.font.Font(None, 20)


@dataclass(slots=True)
class Point:
    x: int = 0
    y: int = 0

    def __add__(self, other):
        return Point(x=self.x + other.x, y=self.y + other.y)

    def __iadd__(self, other):
        return Point(x=self.x + other.x, y=self.y + other.y)

    def __mul__(self, scalar: float | int):
        return Point(x=self.x * scalar, y=self.y * scalar)

    def __imul__(self, scalar: float | int):
        return Point(x=self.x * scalar, y=self.y * scalar)


NORTH = Point(0, -1)
SOUTH = Point(0, 1)
EAST = Point(1, 0)
WEST = Point(-1, 0)


@dataclass(slots=True)
class Snake:
    head: Point
    direction: Point = EAST
    whole: list[Point] = field(default_factory=list)

    def __post_init__(self) -> None:
        self.whole.extend(
            [
                self.head,
                Point(self.head.x - BLOCK_SIZE, self.head.y),
                Point(self.head.x - 2 * BLOCK_SIZE, self.head.y),
            ]
        )

    def update_dir(self, action: np.array) -> None:
        if np.array_equal(action, np.array([1, 0, 0])):
            new_x = self.direction.y
            new_y = -1 * self.direction.x
            self.direction = Point(new_x, new_y)
        elif np.array_equal(action, np.array([0, 0, 1])):
            new_x = -1 * self.direction.y
            new_y = self.direction.x
            self.direction = Point(new_x, new_y)

    def move(self) -> None:
        self.head += self.direction * BLOCK_SIZE
        self.whole.insert(0, self.head)


class SnakeGameAI:
    def __init__(self) -> None:
        self.w: int = 1280
        self.h: int = 960
        self.clock: pg.time.Clock = pg.time.Clock()
        self.reset()

    def reset(self) -> None:
        self.running: bool = True
        self.reward = 0
        self.score: int = 0
        self.speed: int = 100
        self.snake: Snake = Snake(head=Point(self.w // 2, self.h // 2))
        self._place_food()
        self.frame: int = 0
        self.display = pg.display.set_mode((self.w, self.h))
        pg.display.set_caption("Snake")

    def _place_food(self) -> None:
        self.food = Point(
            x=np.random.randint(low=0, high=(self.w - BLOCK_SIZE) // BLOCK_SIZE)
            * BLOCK_SIZE,
            y=np.random.randint(low=0, high=(self.h - BLOCK_SIZE) // BLOCK_SIZE)
            * BLOCK_SIZE,
        )
        if self.food in self.snake.whole:
            self._place_food()

    def _update_ui(self):
        self.display.fill(BLACK)
        for point in self.snake.whole:
            pg.draw.rect(
                self.display, BLUE1, pg.Rect(point.x, point.y, BLOCK_SIZE, BLOCK_SIZE)
            )
        pg.draw.rect(
            self.display, RED, pg.Rect(self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE)
        )
        text = font.render(f"Score: {self.score}", True, WHITE)
        self.display.blit(text, [0, 0])
        pg.display.flip()

    def check_collision(self, point: Optional[Point] = None) -> bool:
        if point is None:
            point = self.snake.head
        return (
            not (
                0 < point.x < self.w - BLOCK_SIZE and 0 < point.y < self.h - BLOCK_SIZE
            )
            or point in self.snake.whole[1:]
        )

    def step(self, action: np.array) -> int:
        self.frame += 1
        # 1. Collect user input
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.running = False
        self.snake.update_dir(action)
        # 2. Move
        self.snake.move()
        # 3. Check if game over
        if self.check_collision() or self.frame > 100 * len(self.snake.whole):
            self.reward = -10
            self.running = False
        # 4. Place new food if eaten
        if self.snake.head == self.food:
            self.reward = 10
            self.score += 1
            self._place_food()
        else:
            self.snake.whole.pop()

        # 5. Update UI and clock
        self._update_ui()
        self.clock.tick(self.speed)
        # 6. Return score
        return self.score, self.reward, self.running

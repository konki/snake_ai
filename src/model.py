import os
from typing import Protocol

import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras.losses import MeanSquaredError
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.optimizers import Adam

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


class Model(Protocol):
    def predict(self, state: np.array, verbose: int) -> None:
        ...

    def predict_on_batch(self, state: np.array, verbose: int) -> None:
        ...

    def fit(self, state: np.array, target: np.array, epochs: int, verbose: int) -> None:
        ...

    def save(self, filename: str, directory: str) -> None:
        ...


def Trainer(Protocol):
    def train_step(
        self,
        state: np.array,
        action: np.array,
        reward: int,
        next_state: np.array,
        running: bool,
    ):
        ...


class LinearQNet:
    def __init__(
        self,
        input_size: int,
        hidden_size: int,
        output_size: int,
        load: bool = True,
        model_loc: str = "models/model.h5",
    ) -> None:
        if load and os.path.exists(model_loc):
            self.net = load_model(model_loc)
        else:
            self.net = Sequential()
            self.net.add(
                Dense(input_size, input_shape=(input_size,), activation="relu")
            )
            self.net.add(Dense(hidden_size, activation="relu"))
            self.net.add(Dense(output_size, activation="softmax"))

    def predict(self, state: np.array, verbose: int = 0) -> np.array:
        return self.net.predict(state, verbose=verbose)

    def predict_on_batch(self, state: np.array) -> np.array:
        return self.net.predict_on_batch(state)

    def fit(
        self, state: np.array, target: np.array, epochs: int = 1, verbose: int = 0
    ) -> None:
        self.net.fit(state, target, epochs=epochs, verbose=verbose)

    def save(self, filename: str = "model.h5", directory: str = "models") -> None:
        file_loc = os.path.join(directory, filename)
        if not os.path.isfile(file_loc):
            self.net.save(file_loc)


class QTrainer:
    def __init__(self, model: Model, lr: float, gamma: float) -> None:
        self.lr = lr
        self.gamma = gamma
        self.optimizer = Adam(learning_rate=self.lr)
        self.loss_function = MeanSquaredError()
        self.model = model
        self.model.net.compile(optimizer=self.optimizer, loss=self.loss_function)
        self.model.net.summary()

    def train_step(
        self,
        state: np.array,
        action: np.array,
        reward: int,
        next_state: np.array,
        running: bool,
    ):
        state = np.array(state)
        next_state = np.array(next_state)
        action = np.array(action)
        reward = np.array(reward)
        running = np.array(running)
        verbose = 0
        if len(state.shape) == 1:
            state = state.reshape((1, -1))
            next_state = next_state.reshape((1, -1))
            action = action.reshape((1, -1))
            reward = np.array([reward]).reshape((1, -1))
            running = np.array([running]).reshape((1, -1))
        else:
            verbose = 1
        # 1: Predicted Q value
        pred = self.model.predict_on_batch(state)

        # 2: Q_new r + gamma * max(Q_pred)
        target = reward + self.gamma * np.amax(
            self.model.predict_on_batch(next_state), axis=1
        ) * (1 - running)

        idx = np.arange(0, len(state), 1)
        pred[idx, np.argmax(action)] = target

        self.model.fit(state, pred, epochs=1, verbose=0)

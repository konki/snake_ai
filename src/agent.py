import os
import random as rand
from collections import deque

import numpy as np
import tensorflow as tf

from ai_snake_game import BLOCK_SIZE, EAST, NORTH, SOUTH, WEST, Point, SnakeGameAI
from helper import plot
from model import LinearQNet, Model, QTrainer, Trainer

gpus = tf.config.list_physical_devices("GPU")
if gpus:
    # Restrict TensorFlow to only use the first GPU
    try:
        tf.config.set_visible_devices(gpus[0], "GPU")
        logical_gpus = tf.config.list_logical_devices("GPU")
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
    except RuntimeError as e:
        # Visible devices must be set before GPUs have been initialized
        print(e)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

MAX_MEMORY = 100_000
BATCH_SIZE = 1000
LR = 0.001


class Agent:
    def __init__(self) -> None:
        self.n_games: int = 0
        self.epsilon: float = 0
        self.gamma: float = 0.9
        self.memory: deque = deque(maxlen=MAX_MEMORY)
        self.model: Model = LinearQNet(input_size=11, hidden_size=256, output_size=3)
        self.trainer: Trainer = QTrainer(model=self.model, lr=LR, gamma=self.gamma)

    def get_state(self, game: SnakeGameAI) -> np.array:
        head = game.snake.head
        block_left = (
            head
            + Point(game.snake.direction.y, -1 * game.snake.direction.x) * BLOCK_SIZE
        )
        block_ahead = head + game.snake.direction * BLOCK_SIZE
        block_right = (
            head
            + Point(-1 * game.snake.direction.y, game.snake.direction.x) * BLOCK_SIZE
        )

        state = np.array(
            [
                # Danger left
                game.check_collision(block_left),
                # Danger ahead
                game.check_collision(block_ahead),
                # Danger right
                game.check_collision(block_right),
                # Direction north
                game.snake.direction == NORTH,
                # Direction south
                game.snake.direction == SOUTH,
                # Direction east
                game.snake.direction == EAST,
                # Direction west
                game.snake.direction == WEST,
                # Food north
                head.y > game.food.y,
                # Food south
                head.y < game.food.y,
                # Food east
                head.x < game.food.x,
                # Food west
                head.x > game.food.x,
            ],
            dtype=int,
        )
        return state

    def remember(
        self,
        state: np.array,
        action: np.array,
        reward: int,
        next_state: np.array,
        running: bool,
    ) -> None:
        self.memory.append((state, action, reward, next_state, running))

    def train_long_memory(self) -> None:
        if len(self.memory) > BATCH_SIZE:
            sample = np.array(rand.sample(self.memory, BATCH_SIZE))
        else:
            sample = self.memory
        states, actions, rewards, next_states, runnings = zip(*sample)
        self.trainer.train_step(states, actions, rewards, next_states, runnings)

    def train_short_memory(
        self,
        state: np.array,
        action: np.array,
        reward: int,
        next_state: np.array,
        running: bool,
    ) -> None:
        self.trainer.train_step(state, action, reward, next_state, running)

    def get_action(self, state: np.array) -> np.array:
        self.epsilon = 80 - self.n_games
        action = np.zeros(3)
        if rand.randint(0, 200) < self.epsilon:
            idx = rand.randint(0, 2)
            action[idx] = 1
        else:
            state = state.reshape((1, -1))
            idx = np.argmax(self.model.predict(state))
            action[idx] = 1
        return action


def train(max_games: int = 100_000):
    scores: np.array = np.array([])
    mean_scores: np.array = np.array([])
    total_score: int = 0
    record: int = 0
    agent: Agent = Agent()
    game: SnakeGameAI = SnakeGameAI()
    while agent.n_games <= max_games:
        current_state = agent.get_state(game)
        action = agent.get_action(current_state)

        score, reward, running = game.step(action)

        new_state = agent.get_state(game)

        agent.train_short_memory(current_state, action, reward, new_state, running)
        agent.remember(current_state, action, reward, new_state, running)

        if not running:
            game.reset()
            agent.n_games += 1
            agent.train_long_memory()
            if score >= record:
                record = score
                agent.model.save()
            print(f"Game nr.: {agent.n_games}, Score: {score}, Record: {record}")
            scores = np.append(scores, score)
            mean_scores = np.append(mean_scores, np.mean(scores))
            plot(scores, mean_scores)


if __name__ == "__main__":
    train()

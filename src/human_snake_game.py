from dataclasses import dataclass, field
from enum import Enum, auto

import numpy as np
import pygame as pg

pg.init()

BLOCK_SIZE: int = 40

# Colors
WHITE: tuple = (255, 255, 255)
BLACK: tuple = (0, 0, 0)
RED: tuple = (255, 0, 0)
GREEN: tuple = (0, 255, 0)
BLUE: tuple = (0, 0, 255)
BLUE1: tuple = (0, 100, 255)

# Font
font = pg.font.Font(None, 20)


@dataclass(slots=True)
class Point:
    x: int = 0
    y: int = 0

    def __add__(self, other):
        return Point(x=self.x + other.x, y=self.y + other.y)

    def __iadd__(self, other):
        return Point(x=self.x + other.x, y=self.y + other.y)

    def __mul__(self, scalar: float | int):
        return Point(x=self.x * scalar, y=self.y * scalar)

    def __imul__(self, scalar: float | int):
        return Point(x=self.x * scalar, y=self.y * scalar)


UP = Point(0, -1)
DOWN = Point(0, 1)
RIGHT = Point(1, 0)
LEFT = Point(-1, 0)


@dataclass(slots=True)
class Snake:
    head: Point
    direction: Point = RIGHT
    whole: list[Point] = field(default_factory=list)

    def __post_init__(self) -> None:
        self.whole.extend(
            [
                self.head,
                Point(self.head.x - BLOCK_SIZE, self.head.y),
                Point(self.head.x - 2 * BLOCK_SIZE, self.head.y),
            ]
        )

    def update_dir(self, new_dir: Point) -> None:
        if (
            not np.abs(
                (self.direction.x + new_dir.x) ** 2
                + (self.direction.y + new_dir.y) ** 2
            )
            == 0
        ):
            self.direction = new_dir

    def move(self) -> None:
        self.head += self.direction * BLOCK_SIZE
        self.whole.insert(0, self.head)


@dataclass(slots=True)
class SnakeGame:
    w: int = 1280
    h: int = 960
    running: bool = True
    score: int = 0
    speed: int = 15
    clock: pg.time.Clock = pg.time.Clock()

    snake: Snake = field(init=False)
    food: Point = field(init=False)
    display: pg.display = field(init=False)

    def __post_init__(self) -> None:
        self.snake = Snake(head=Point(self.w // 2, self.h // 2))
        self._place_food()
        self.display = pg.display.set_mode((self.w, self.h))
        pg.display.set_caption("Snake")
        print(self.snake)

    def _place_food(self) -> None:
        self.food = Point(
            x=np.random.randint(low=0, high=(self.w - BLOCK_SIZE) // BLOCK_SIZE)
            * BLOCK_SIZE,
            y=np.random.randint(low=0, high=(self.h - BLOCK_SIZE) // BLOCK_SIZE)
            * BLOCK_SIZE,
        )
        if self.food in self.snake.whole:
            self._place_food()

    def _update_ui(self):
        self.display.fill(BLACK)
        for point in self.snake.whole:
            pg.draw.rect(
                self.display, BLUE1, pg.Rect(point.x, point.y, BLOCK_SIZE, BLOCK_SIZE)
            )
        pg.draw.rect(
            self.display, RED, pg.Rect(self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE)
        )
        text = font.render(f"Score: {self.score}", True, WHITE)
        self.display.blit(text, [0, 0])
        pg.display.flip()

    def _check_collision(self):
        if not (
            0 < self.snake.head.x < self.w - BLOCK_SIZE
            and 0 < self.snake.head.y < self.h - BLOCK_SIZE
        ):
            self.running = False
        if self.snake.head in self.snake.whole[1:]:
            print("Hit yourself")
            print(self.snake.whole)
            self.running = False

    def step(self) -> int:
        # 1. Collect user input
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.running = False
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_LEFT or event.key == pg.K_a:
                    self.snake.update_dir(LEFT)
                elif event.key == pg.K_RIGHT or event.key == pg.K_d:
                    self.snake.update_dir(RIGHT)
                elif event.key == pg.K_UP or event.key == pg.K_w:
                    self.snake.update_dir(UP)
                elif event.key == pg.K_DOWN or event.key == pg.K_s:
                    self.snake.update_dir(DOWN)
        print(self.snake.direction)
        # 2. Move
        self.snake.move()
        # 3. Check if game over
        self._check_collision()
        # 4. Place new food if eaten
        if self.snake.head == self.food:
            self.score += 1
            self._place_food()
        else:
            self.snake.whole.pop()

        # 5. Update UI and clock
        self._update_ui()
        self.clock.tick(self.speed)
        # 6. Return score
        return self.score


def main() -> None:
    game = SnakeGame()
    while game.running:
        score = game.step()
    print(f"Final score: {score}")
    pg.quit()


if __name__ == "__main__":
    main()
